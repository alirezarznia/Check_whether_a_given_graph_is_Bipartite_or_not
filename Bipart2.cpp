#include <cstdio>
#include <vector>
#include <queue>
#include <iostream>
using namespace std;

#define MAX 1000

int numberVertex, numberEdges;
int particion[MAX], visited[MAX];
vector< int > adjacencyMatrix[MAX];

bool bfs()
{
    int i, origin, destination, begin;
    queue< int > queueVertex;
    begin = 0;
    queueVertex.push(begin);
    particion[begin] = 1; // 1 left,
    visited[begin] = 1; // set adjacencyMatrixray

    while(!queueVertex.empty())
    {
        origin = queueVertex.front(); queueVertex.pop();
        for(i=0; i < adjacencyMatrix[origin].size(); i++)
        {
            destination = adjacencyMatrix[origin][i];
            if(particion[origin] == particion[destination])
            {
                return false;
            }
            if(visited[destination] == 0)
            {
                visited[destination] = 1;
                particion[destination] = 3 - particion[origin]; // alter 1 and 2 subsets
                queueVertex.push(destination);
            }
        }
    }
    return true;
}

int main()
{
 freopen("tarea2.in", "r", stdin);
    int i,j, nodeOrigin, nodeDestination;
    scanf("%d %d", &numberVertex, &numberEdges);
    for(i=0; i<numberEdges; i++)
    {
        scanf("%d %d", &nodeOrigin, &nodeDestination);
        adjacencyMatrix[nodeOrigin].push_back(nodeDestination);
        adjacencyMatrix[nodeDestination].push_back(nodeOrigin);
    }
    if(bfs()) {

        printf("Is bipartite\n");
          for (j=0; j<numberVertex; j++){
        cout<<j<<" "<<particion[j]<<endl;
        }

    }
    else {printf("Is not bipartite\n");}





    return 0;
}