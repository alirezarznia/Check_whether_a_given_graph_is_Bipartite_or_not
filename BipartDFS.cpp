bool bipartite = true;
int a[MaxN][MaxN], color[MaxN];

void dfs(int v, int cur){
  mark[v] = true;
  color[v] = cur; // color this vertex as cur
  for (int i = 0; i < n; i++)
     if (a[v][i] == 1){ // if there is edge between v and i
         if (color[i] == cur) { // if color of vertex i is equal to color of v, that is cur
               bipartite = false; // graph is definitely not bipartite, so return
               return;
         }
         if (!mark[i]) dfs(v, cur==1?2:1); // continue dfs
     }
};

int main(){
...
dfs(0, 1); 
cout << bipartite;
...
}